import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import Navigation from "./src/routes";
import { Provider } from "react-redux";
import { createStore } from "redux";
import CatsReducer from "./src/reduxApp/cats/reducers";

const store = createStore(CatsReducer);

export default function App() {
  return (
    <Provider store={store}>
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Navigation />
        </View>
      </SafeAreaView>
    </Provider>
  );
}
