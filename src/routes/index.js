import React from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import { NavigationContainer } from "@react-navigation/native";

import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "../screens/Home";
import CatsDetails from "../screens/CatsDetails";
import AddCats from "../screens/AddCats";
import EditCats from "../screens/EditCats";

const Stack = createNativeStackNavigator();

export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Home"
      >
        <Stack.Screen component={Home} name="Home" />
        <Stack.Screen component={CatsDetails} name="CatsDetails" />
        <Stack.Screen component={AddCats} name="AddCats" />
        <Stack.Screen component={EditCats} name="EditCats" />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
