import Cat1 from "../assets/images/cat1.png";
import Cat2 from "../assets/images/cat2.png";
import Cat3 from "../assets/images/cat3.png";
import Cat4 from "../assets/images/cat4.png";
import Cat5 from "../assets/images/cat5.png";

export const getCatsImages = () => {
  const catImages = [
    {
      name: "cat1",
      image: Cat1,
    },
    {
      name: "cat2",
      image: Cat2,
    },
    {
      name: "cat3",
      image: Cat3,
    },
    {
      name: "cat4",
      image: Cat4,
    },
    {
      name: "cat5",
      image: Cat5,
    },
  ];

  return catImages[Math.floor(Math.random() * 5)];
};
