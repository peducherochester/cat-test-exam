import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet } from "react-native";

import { Input, Button, PickerComponent } from "./index";

export const Form = (props) => {
  const { handleCatAction, catEditData, buttonType } = props;
  const [catAddedData, setCatAddedData] = useState({
    name: "",
    color: "",
    gender: "",
    size: "",
    weight: "",
    description: "",
  });
  const [catAdded, setCatAdded] = useState(false);

  const [catEditedData, setCatEditedData] = useState({});

  useEffect(() => {
    catEditData &&
      setCatAddedData({
        name: catEditData.name,
        color: catEditData.color,
        gender: catEditData.gender,
        size: catEditData.size,
        weight: catEditData.weight,
        description: catEditData.description,
      });
  }, [catEditData]);

  const handleInputChange = (type, value) => {
    setCatAddedData({
      ...catAddedData,
      [type.toString().toLowerCase()]: value,
    });
    setCatEditedData({
      ...catEditedData,
      [type.toString().toLowerCase()]: value,
    });
    setCatAdded(false);
  };

  const handlePress = () => {
    const dataType = buttonType === "ADD" ? catAddedData : catEditedData;

    handleCatAction({
      ...dataType,
      ...(buttonType === "ADD" && {
        catId: `${catAddedData?.name}-${Math.floor(
          Math.random() * 300
        )}-${Math.floor(Math.random() * 300)}-${Math.floor(
          Math.random() * 300
        )}`,
      }),
    });
    setCatAdded(true);
    buttonType === "ADD" &&
      setCatAddedData({
        name: "",
        gender: "",
        size: "",
        weight: "",
        description: "",
        color: "",
      });
  };

  const disable =
    catAddedData?.name?.length > 0 &&
    catAddedData?.color?.length > 0 &&
    catAddedData?.gender?.length > 0 &&
    catAddedData?.size?.length > 0 &&
    catAddedData?.weight?.length > 0;

  useEffect(() => {}, []);

  return (
    <>
      <View style={styles.FormContainer}>
        <View>
          <Text style={{ color: "#6af56a", fontSize: 14, fontWeight: "700" }}>
            {catAdded
              ? buttonType === "ADD"
                ? "Successfully Added!"
                : "SAVED!"
              : ""}
          </Text>
        </View>
        <Input
          handleInputChange={handleInputChange}
          inputValue={catAddedData.name}
          inputLabel={"Name"}
        />
        <Input
          handleInputChange={handleInputChange}
          inputValue={catAddedData.color}
          inputLabel={"Color"}
        />
        <PickerComponent
          pickerLabel={"Gender"}
          pickerValue={catAddedData.gender}
          pickerItems={[
            { name: "Male", value: "male" },
            { name: "Female", value: "female" },
          ]}
          setSelectedValue={handleInputChange}
        />
        <PickerComponent
          pickerLabel={"Size"}
          pickerValue={catAddedData.size}
          pickerItems={[
            { name: "Small", value: "small" },
            { name: "Medium", value: "medium" },
            { name: "Large", value: "large" },
          ]}
          setSelectedValue={handleInputChange}
        />

        <Input
          handleInputChange={handleInputChange}
          inputValue={catAddedData.weight}
          inputLabel={"Weight"}
        />
        <Input
          keyboardType={"number-pad"}
          handleInputChange={handleInputChange}
          inputValue={catAddedData.description}
          inputLabel={"Description"}
        />
        <Button
          disabled={!disable}
          handlePress={handlePress}
          buttonLabel={buttonType}
          style={{
            button: styles.ButtonStyle,
            textStyle: styles.ButtonLabelStyle,
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  FormContainer: {
    width: "100%",
    flex: 1,
    marginTop: 90,
    paddingHorizontal: 25,
    display: "flex",
  },
  ButtonStyle: {
    backgroundColor: "green",
    width: 130,
    textAlign: "center",
    paddingVertical: 15,
    borderRadius: 5,
    marginTop: 15,
    alignSelf: "flex-end",
  },
  ButtonLabelStyle: {
    color: "white",
    fontSize: 17,
    fontWeight: "700",
  },
});
