import { Picker } from "@react-native-picker/picker";
import React from "react";
import { Text, View } from "react-native";

export const PickerComponent = (props) => {
  const { pickerValue, pickerItems, setSelectedValue, pickerLabel } = props;

  return (
    <>
      <Text style={{ color: "white", marginTop: 8, marginBottom: 5 }}>
        {pickerLabel} : {pickerValue?.toUpperCase()}
      </Text>

      <Picker
        selectedValue={pickerValue}
        // value={pickerValue}
        onFocus={() => setSelectedValue(`Select ${pickerLabel}`)}
        style={{ marginBottom: 5 }}
        onValueChange={(itemValue, itemIndex) =>
          setSelectedValue(pickerLabel.toString().toLowerCase(), itemValue)
        }
      >
        <Picker.Item value="" label={`Select ${pickerLabel}`} />
        {pickerItems.map((item) => {
          return (
            <Picker.Item key={item.name} label={item.name} value={item.value} />
          );
        })}
      </Picker>

      <Text style={{ color: "red" }}>
        {pickerValue?.length <= 0 ? "Pick another value" : ""}
      </Text>
    </>
  );
};
