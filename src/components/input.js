import React, { useEffect, useState } from "react";
import { Text, TextInput, View } from "react-native";

export const Input = (props) => {
  const {
    inputLabel,
    inputValue,
    handleInputChange,
    inputError,
    keyboardType,
  } = props;

  return (
    <>
      <Text style={{ color: "white", marginTop: 8 }}>{inputLabel}</Text>
      <TextInput
        keyboardType={keyboardType}
        value={inputValue}
        onChangeText={(value) => handleInputChange(inputLabel, value)}
        style={{
          color: "black",
          backgroundColor: "white",
          marginBottom: 12,
          marginTop: 5,
          paddingLeft: 5,
        }}
      />
      <Text>{inputError}</Text>
    </>
  );
};
