import React, { useEffect, useState } from "react";
import { Text, TouchableOpacity } from "react-native";

export const Button = (props) => {
  const { buttonLabel, style, handlePress, disabled } = props;
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={() => handlePress()}
      style={[style.button, { backgroundColor: disabled ? "gray" : "green" }]}
    >
      <Text style={style.textStyle}>{buttonLabel}</Text>
    </TouchableOpacity>
  );
};
