import { combineReducers } from "redux";

const INITIAL_STATE = {
  catsLists: [
    {
      name: "White Fang",
      color: "orange",
      gender: "Male",
      weight: "25kg",
      size: "Medium",
      breed: "Persian",
      description: "white fang is dangerous",
      catId: "White-Fang-1",
    },
    {
      name: "Yellow Tail",
      color: "yellow",
      gender: "Male",
      weight: "25kg",
      size: "Medium",
      breed: "Persian",
      description: "Yellow Tail has no tail",
      catId: "Yellow-Tail-2",
    },
    {
      name: "Orange Paw",
      color: "orange",
      gender: "Male",
      weight: "25kg",
      size: "Medium",
      breed: "Persian",
      description: "Orange Paw has one eye",
      catId: "Orange-Paw-3",
    },
    {
      name: "Red Ear",
      color: "red",
      gender: "Male",
      weight: "25kg",
      size: "Medium",
      breed: "Persian",
      description: "Red Ear is red",
      catId: "Red-Ear-4",
    },
  ],
  currentCat: {},
};

const catsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "GET_CATS":
      return state;

    case "ADD_CAT":
      return {
        catsLists: [...state.catsLists, action.payload],
      };

    case "DELETE_CAT":
      const catReturn = state.catsLists.filter(
        (cats) => cats.catId != action.payload
      );
      return {
        catsLists: catReturn,
      };
    case "EDIT_CAT":
      const newState = state.catsLists.filter((item) => {
        return item.catId != action?.payload?.catOriginalDetails?.catId;
      });

      const newEditedData = {
        ...action.payload?.catOriginalDetails,
        ...action?.payload?.editedData,
      };

      return { catsLists: [...newState, newEditedData] };

    default:
      return state;
  }
};

export default combineReducers({
  cats: catsReducer,
});
