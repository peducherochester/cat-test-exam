import { createStore } from "redux";
import catsReducer from "./reducers";
const store = createStore(catsReducer);
export default store;
