export const getCats = (catsData) => ({
  type: "GET_CATS",
  payload: catsData,
});

export const addCat = (catInfo) => ({
  type: "ADD_CAT",
  payload: catInfo,
});

export const editCat = (catId) => ({
  type: "EDIT_CAT",
  payload: catId,
});

export const deleteCat = (catId) => ({
  type: "DELETE_CAT",
  payload: catId,
});
