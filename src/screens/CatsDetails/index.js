import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Back from "../../assets/icons/back.png";
import { deleteCat, getCats } from "../../reduxApp/cats/actions";
import { useDispatch, useSelector } from "react-redux";

const CatsDetails = (props) => {
  const { route, navigation } = props;
  const dispatch = useDispatch();
  const deleteFunction = () => {
    dispatch(deleteCat(route?.params?.catDetails.catId));
    setTimeout(() => {
      dispatch(getCats());
      navigation.goBack();
    }, 500);
  };

  const cats = useSelector((state) => state.cats);

  console.log(cats, "detailspage");

  const [getCurrentCat, setGetCurrentCat] = useState("");

  useEffect(() => {
    if (cats?.catsLists?.length) {
      const current = cats?.catsLists.filter((item) => {
        return item.catId === route?.params?.catDetails.catId;
      });

      setGetCurrentCat(current[0]);
    }
  }, [cats?.catsLists]);

  return (
    <View style={styles.DetailsContainer}>
      <View
        style={{
          position: "absolute",
          top: 15,
          left: 10,
          zIndex: 2,
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image style={{ height: 60, width: 60 }} source={Back} />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <Text style={styles.CatName}>{getCurrentCat?.name}</Text>
        <View style={styles.ImageHolder}>
          <Image style={styles.catImage} source={route?.params?.image} />
        </View>
        <View style={{ marginLeft: 15 }}>
          <Text style={styles.CatDetails}>Breed: {getCurrentCat?.breed}</Text>
          <Text style={styles.CatDetails}>Color: {getCurrentCat?.color}</Text>
          <Text style={styles.CatDetails}>Gender: {getCurrentCat?.gender}</Text>
          <Text style={styles.CatDetails}>Size: {getCurrentCat?.size}</Text>
          <Text style={styles.CatDetails}>Weight: {getCurrentCat?.weight}</Text>
          <Text style={styles.CatDetails}>
            Description: {getCurrentCat?.description}
          </Text>
        </View>
        <View style={styles.ButtonsHolder}>
          <TouchableOpacity
            onPress={() =>
              navigation.push("EditCats", { catDetails: getCurrentCat })
            }
            style={[styles.ButtonStyle, { backgroundColor: "green" }]}
          >
            <Text style={styles.ButtonText}>EDIT</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => deleteFunction()}
            style={[styles.ButtonStyle, { backgroundColor: "red" }]}
          >
            <Text style={styles.ButtonText}>DELETE</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default CatsDetails;

const styles = StyleSheet.create({
  DetailsContainer: {
    backgroundColor: "black",
    height: "100%",
    width: "100%",
  },
  CatName: {
    color: "white",
    fontSize: 34,
    fontWeight: "800",
    marginVertical: 20,
    marginLeft: 10,
    marginTop: 90,
  },
  ImageHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  catImage: {
    height: 375,
    width: 375,
  },
  CatDetails: {
    color: "white",
    fontSize: 20,
    fontWeight: "700",
    marginVertical: 15,
  },
  ButtonsHolder: {
    display: "flex",
    flexDirection: "row",
    height: 150,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  ButtonStyle: {
    height: 75,
    width: 150,
    borderRadius: 12,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  ButtonText: {
    color: "white",
    fontSize: 20,
    fontWeight: "600",
  },
});
