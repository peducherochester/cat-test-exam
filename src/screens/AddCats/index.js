import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import Back from "../../assets/icons/back.png";
import { useDispatch } from "react-redux";
import { Form } from "../../components";
import { addCat, getCats } from "../../reduxApp/cats/actions";

const CatsDetails = (props) => {
  const { route, navigation } = props;
  const dispatch = useDispatch();

  const handleCatAction = (catData) => {
    dispatch(addCat(catData));
    dispatch(getCats());
  };

  return (
    <View style={styles.DetailsContainer}>
      <View
        style={{
          position: "absolute",
          top: 15,
          left: 10,
          zIndex: 2,
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Image style={{ height: 60, width: 60 }} source={Back} />
        </TouchableOpacity>
      </View>
      <Form buttonType={"ADD"} handleCatAction={handleCatAction} />
    </View>
  );
};

export default CatsDetails;

const styles = StyleSheet.create({
  DetailsContainer: {
    backgroundColor: "black",

    width: "100%",
    paddingBottom: 25,
    flex: 1,
  },
});
