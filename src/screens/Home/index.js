import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { getCats } from "../../reduxApp/cats/actions";
import CatIcon from "../../assets/icons/paw.png";
import { getCatsImages } from "../../utils/getCatsImages";

const Home = (props) => {
  const { navigation } = props;

  const cats = useSelector((state) => state.cats);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCats());
  }, []);

  const renderItem = ({ item }) => {
    const catimage = getCatsImages().image;
    return (
      <View style={styles.CatContainerHolder}>
        <TouchableOpacity
          onPress={() =>
            navigation.push("CatsDetails", {
              catDetails: item,
              image: catimage,
            })
          }
          style={styles.CatContainer}
        >
          <Text style={styles.CatName}>{item.name}</Text>
          <View style={styles.CatInfoHolder}>
            <Image style={styles.CatImages} source={catimage} />
            <Text style={styles.CatName}>Pick Me</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{ backgroundColor: "black", height: "100%", width: "100%" }}>
      <View style={styles.IconHolder}>
        <Image style={styles.CatIcon} source={CatIcon} />
        <View
          style={{
            position: "absolute",
            top: 15,
            right: 30,
            zIndex: 2,
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.push("AddCats")}
            style={{
              backgroundColor: "#df3535",
              padding: 10,
              borderRadius: 12,
            }}
          >
            <Text style={{ color: "white", fontSize: 17, fontWeight: "700" }}>
              Add Cats
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ marginBottom: 30 }} />
      {cats?.catsLists?.length > 0 && (
        <FlatList
          data={cats?.catsLists}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        />
      )}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  IconHolder: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
  },
  CatIcon: {
    height: 55,
    width: 55,
  },
  CatContainerHolder: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  CatContainer: {
    width: "100%",
    marginVertical: 15,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderColor: "white",
    width: "65%",
    padding: 25,
    borderRadius: 12,
  },
  CatName: {
    color: "white",
    fontSize: 20,
    fontWeight: "800",
    marginBottom: 25,
  },
  CatInfoHolder: {
    display: "flex",
    alignItems: "center",
  },
  CatImages: {
    height: 150,
    width: 150,
  },
});
